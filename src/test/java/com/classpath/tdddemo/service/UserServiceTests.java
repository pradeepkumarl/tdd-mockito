package com.classpath.tdddemo.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.classpath.tdddemo.model.User;
import com.classpath.tdddemo.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
	
	@Mock
	private UserRepository userRepository;
	
	@InjectMocks
	private UserService userService;
	
	
	@Test
	void testSaveUser() {
		
		//set the expectation
		when(this.userRepository.saveUser(any(User.class))).thenReturn(new User(12, "Neeraj", 34));
		
		//execute the method
		User savedUser = this.userService.saveUser(new User(1, "Neeraj", 35));
		
		//assertions
		Assertions.assertNotNull(savedUser);
		Assertions.assertEquals(12, savedUser.getId());
		
	}
	
	@Test
	void testSaveUserInvalid() {
		
		//set the expectation
		lenient().when(this.userRepository.saveUser(any(User.class))).thenReturn(new User(12, "Neeraj", 34));
		
		//execute the method
		User savedUser = this.userService.saveUser(new User(1, "Neeraj", 13));
		
		//assertions
		Assertions.assertNull(savedUser);
		
	}

}
