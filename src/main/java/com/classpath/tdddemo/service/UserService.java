package com.classpath.tdddemo.service;

import org.springframework.stereotype.Service;

import com.classpath.tdddemo.model.User;
import com.classpath.tdddemo.repository.UserRepository;

@Service
public class UserService {

	private UserRepository useRepository;

	public User saveUser(User user) {

		if (validateUser(user)) {
			return this.useRepository.saveUser(user);
		}
		return null;
	}

	private boolean validateUser(User user) {
		if (user.getAge() < 18) {
			return false;
		}
		return true;

	}

}
